const net = require("net");
const dns = require("dns");
const socks = require('socksv5');

export default class MailChecker {

    static check(email, result, proxy) {
        return new Promise(function(response, reject) {
            
            dns.resolveMx(email.split('@')[1], function(err, addresses) {
                if(!addresses || addresses.length <=0)
                    return reject(result);
                let client = null;
                let commands = ["helo " + addresses[0].exchange, "mail from: <" + email + ">", "rcpt to: <" + email + ">"];
                let i = 0;
                result.mx_domains = addresses;

                if (proxy) {
                    client = socks.connect({
                        host: addresses[0].exchange,
                        port: 25,
                        proxyHost: proxy.host,
                        proxyPort: proxy.port,
                        auths: [socks.auth.None()]
                    });
                }
                else {
                    client = net.createConnection(25, addresses[0].exchange);
                    client.setTimeout(5000);
                }



                client.on('connect', function(proxysocket) {

                    result.mx_exists = true;

                    if (proxysocket)
                        client = proxysocket;

                    client.on('prompt', function() {

                        if (i < 3) {
                            client.write(commands[i]);
                            client.write('\r\n');
                            i++;
                        }
                        else {
                            result.server_is_online = true;
                            result.address_exists = true;
                            response(result);

                            client.removeAllListeners();
                            client.destroy(); //destroy socket manually

                        }
                    });

                    client.on('undetermined', function() {
                        result.server_is_online = false;
                        client.removeAllListeners();
                        client.destroy();
                        reject(result);

                    });

                    client.on('timeout', function() {
                        client.emit('undetermined');
                    });

                    client.on('error', function(err) {
                        result.server_is_online = false;
                        client.emit('false');


                    });

                    client.on('false', function() {
                        client.removeAllListeners();
                        client.destroy();
                        reject(result);
                    });



                    client.on('data', function(data) {
                        data = data.toString();
                        result.server_is_online = true;
                        if (data.match(/220 |[^,]*$/g)[1]) {
                            var RESPONSE_220 = data.match(/220 |[^,]*$/g)[1];
                            result.server_helo_response = RESPONSE_220.replace(/(\r\n|\n|\r)/gm,"");
                        }
                        if (data.match(/250 |[^,]*$/g)[1]) {
                            var RESPONSE_250 = data.match(/250 |[^,]*$/g)[1];
                            result.mail_from_response = RESPONSE_250.replace(/(\r\n|\n|\r)/gm,"");
                        }
                        if (data.match(/550 |[^,]*$/g)[1]) {
                            var RESPONSE_550 = data.match(/550 |[^,]*$/g)[1];
                            result.rcpt_to_response = RESPONSE_550.replace(/(\r\n|\n|\r)/gm,"");
                        }
                        if (data.indexOf("220") == 0 || data.indexOf("250") == 0 || data.indexOf("\n220") != -1 || data.indexOf("\n250") != -1) {
                            client.emit('prompt');
                        }
                        else if (data.indexOf("\n550") != -1 || data.indexOf("550") == 0) {

                            client.emit('false');
                        }
                        else {
                            client.emit('undetermined');
                        }
                    });

                });
            });

        });
    }

    static validateEmail(mail) {
        return new Promise(function(response, reject) {
            if (/^\S+@\S+$/.test(mail)) {
                return response(true);
            }
            else {
                return reject("Email not valid");
            }
        })

    }

}
