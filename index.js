import MailChecker from "./mailchecker";

let use_proxy = false;
let proxy_url = "";
let proxy_port = "";
let email = process.argv[2];

let result = {
  address: false,
  mx_exists: false,
  server_is_online: false,
  address_exists: false,
  wrong_address_accepted: false,
  server_helo_response: "",
  mx_domains: [],
  mail_from_response: "",
  rcpt_to_response: "",
}


Main(process.argv);

function Main(args) {
  let proxy = null;
  let index_proxy = args.indexOf("--proxy")
  if (index_proxy > 0) {
    proxy_url = process.argv[index_proxy + 1].split(":")[1].replace("//", "");
    proxy_port = process.argv[index_proxy + 1].split(":")[2];
    proxy = {host:proxy_url,port:proxy_port};
  }

  MailChecker.validateEmail(email).then((valid) => {
    if (valid) {
      result.address = true;
      MailChecker.check(email,result,proxy).then((result)=>{
        console.log(result);
      },
      (error)=>{
        console.log(result);
      });

    }
  }, (error) => {
    console.log(result);

  });
}